package me.xethh.utils.decimalUtils;

import java.math.BigDecimal;

/**
 * @author xethhung
 * Created on 9/20/2018
 */
public class DecimalOperatorImpl implements DecimalOperator{
    private DecimalBuilder builder;
    private BigDecimal decimal;

    private DecimalOperatorImpl(DecimalBuilder builder, BigDecimal bigDecimal){
        this.builder = builder;
        this.decimal = builder.format(bigDecimal);
    }
    protected static DecimalOperator of(DecimalBuilder builder, BigDecimal bigDecimal){
        return new DecimalOperatorImpl(builder,bigDecimal);
    }

    @Override
    public DecimalOperator add(BigDecimal bigDecimal) {
        return new DecimalOperatorImpl(builder,decimal.add(bigDecimal));
    }

    @Override
    public DecimalOperator add(int integer) {
        return add(builder.format(integer));
    }

    @Override
    public DecimalOperator add(long longVal) {
        return add(builder.format(longVal));
    }

    @Override
    public DecimalOperator add(double doubleVal) {
        return add(builder.format(doubleVal));
    }

    @Override
    public DecimalOperator subtract(BigDecimal bigDecimal) {
        return new DecimalOperatorImpl(builder,decimal.subtract(bigDecimal));
    }

    @Override
    public DecimalOperator subtract(int integer) {
        return subtract(builder.format(integer));
    }

    @Override
    public DecimalOperator subtract(long longVal) {
        return subtract(builder.format(longVal));
    }

    @Override
    public DecimalOperator subtract(double doubleVal) {
        return subtract(builder.format(doubleVal));
    }

    public DecimalOperatorImpl multiply(BigDecimal bigDecimal){
        return new DecimalOperatorImpl(builder, decimal.multiply(bigDecimal));
    }

    public DecimalOperatorImpl multiply(int integer){
        return multiply(builder.format(integer));
    }

    public DecimalOperatorImpl multiply(long longVal){
        return multiply(builder.format(longVal));
    }

    public DecimalOperatorImpl multiply(double doubleVal){
        return multiply(builder.format(doubleVal));
    }


    public DecimalOperatorImpl divide(BigDecimal bigDecimal){
        return new DecimalOperatorImpl(builder, decimal.divide(bigDecimal,builder.mode()));
    }

    @Override
    public DecimalOperator divide(int integer) {
        return divide(builder.format(integer));
    }

    public DecimalOperatorImpl divide(long longVal){
        return divide(builder.format(longVal));
    }

    public DecimalOperatorImpl divide(double doubleVal){
        return divide(builder.format(doubleVal));
    }
    public BigDecimal decimal(){
        return decimal;
    }

    @Override
    public boolean eq(int integer) {
        return eq(builder.format(integer));
    }

    @Override
    public boolean eq(long longVal) {
        return eq(builder.format(longVal));
    }

    @Override
    public boolean eq(double doubleVal) {
        return eq(builder.format(doubleVal));
    }

    @Override
    public boolean eq(BigDecimal bigDecimal) {
        return decimal().compareTo(bigDecimal)==0;
    }

    @Override
    public boolean lt(int integer) {
        return lt(integer);
    }

    @Override
    public boolean lt(long longVal) {
        return lt(builder.format(longVal));
    }

    @Override
    public boolean lt(double doubleVal) {
        return lt(builder.format(doubleVal));
    }

    @Override
    public boolean lt(BigDecimal bigDecimal) {
        return decimal().compareTo(bigDecimal)==-1;
    }

    @Override
    public boolean le(int integer) {
        return le(builder.format(integer));
    }

    @Override
    public boolean le(long longVal) {
        return le(builder.format(longVal));
    }

    @Override
    public boolean le(double doubleVal) {
        return le(builder.format(doubleVal));
    }

    @Override
    public boolean le(BigDecimal bigDecimal) {
        return eq(bigDecimal) || lt(bigDecimal);
    }

    @Override
    public boolean gt(int integer) {
        return gt(builder.format(integer));
    }

    @Override
    public boolean gt(long longVal) {
        return gt(builder.format(longVal));
    }

    @Override
    public boolean gt(double doubleVal) {
        return gt(builder.format(doubleVal));
    }

    @Override
    public boolean gt(BigDecimal bigDecimal) {
        return decimal().compareTo(bigDecimal)==1;
    }

    @Override
    public boolean ge(int integer) {
        return ge(builder.format(integer));
    }

    @Override
    public boolean ge(long longVal) {
        return ge(builder.format(longVal));
    }

    @Override
    public boolean ge(double doubleVal) {
        return ge(builder.format(doubleVal));
    }

    @Override
    public boolean ge(BigDecimal bigDecimal) {
        return gt(bigDecimal) || eq(bigDecimal);
    }

    @Override
    public BigDecimal minVal(int integer) {
        return minVal(builder.format(integer));
    }

    @Override
    public BigDecimal minVal(long longVal) {
        return minVal(builder.format(longVal));
    }

    @Override
    public BigDecimal minVal(double doubleVal) {
        return minVal(builder.format(doubleVal));
    }

    @Override
    public BigDecimal minVal(BigDecimal bigDecimal) {
        return decimal().min(bigDecimal);
    }

    @Override
    public BigDecimal maxVal(int integer) {
        return maxVal(builder.format(integer));
    }

    @Override
    public BigDecimal maxVal(long longVal) {
        return maxVal(builder.format(longVal));
    }

    @Override
    public BigDecimal maxVal(double doubleVal) {
        return maxVal(builder.format(doubleVal));
    }

    @Override
    public BigDecimal maxVal(BigDecimal bigDecimal) {
        return decimal().max(bigDecimal);
    }

    @Override
    public DecimalOperator min(int integer) {
        return min(builder.format(integer));
    }

    @Override
    public DecimalOperator min(long longVal) {
        return min(builder.format(longVal));
    }

    @Override
    public DecimalOperator min(double doubleVal) {
        return min(builder.format(doubleVal));
    }

    @Override
    public DecimalOperator min(BigDecimal bigDecimal) {
        return DecimalOperatorImpl.of(builder,minVal(bigDecimal));
    }

    @Override
    public DecimalOperator max(int integer) {
        return max(builder.format(integer));
    }

    @Override
    public DecimalOperator max(long longVal) {
        return max(builder.format(longVal));
    }

    @Override
    public DecimalOperator max(double doubleVal) {
        return max(builder.format(doubleVal));
    }

    @Override
    public DecimalOperator max(BigDecimal bigDecimal) {
        return DecimalOperatorImpl.of(builder,maxVal(bigDecimal));
    }

    @Override
    public boolean isNegative() {
        return decimal().compareTo(BigDecimal.ZERO)==-1;
    }

    @Override
    public DecimalOperator negate() {
        return multiply(-1);
    }

    public String decimalString(){
        return decimal.toPlainString();
    }

    @Override
    public String decimalString(int scale) {
        return DecimalOperatorImpl.of(builder.scale(scale),decimal).decimalString();
    }

    public DecimalBuilder builder() {
        return builder;
    }

    public DecimalOperator builder(DecimalBuilder builder) {
        return new DecimalOperatorImpl(builder,decimal);
    }

    @Override
    public DecimalOperator mutable() {
        return DecimalOperatorMutable.of(this);
    }

    @Override
    public DecimalOperator immutable() {
        return this;
    }

    @Override
    public String toString() {
        return decimalString();
    }
}
