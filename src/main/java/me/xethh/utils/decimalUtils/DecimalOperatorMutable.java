package me.xethh.utils.decimalUtils;

import java.math.BigDecimal;

/**
 * @author xethhung
 * Created on 9/20/2018
 */
public class DecimalOperatorMutable implements DecimalOperator{
    DecimalOperator oper;
    private DecimalOperatorMutable(DecimalOperator oper){
        this.oper = oper;
    }

    public static DecimalOperator of(DecimalOperator oper){
        if(oper instanceof DecimalOperatorImpl)
            return new DecimalOperatorMutable(oper);
        throw  new RuntimeException("DecimalOperatorMutable only support DecimalOperatorImpl");
    }

    @Override
    public DecimalOperator add(BigDecimal bigDecimal) {
        oper = oper.add(bigDecimal);
        return this;
    }

    @Override
    public DecimalOperator add(int integer) {
        oper = oper.add(integer);
        return this;
    }

    @Override
    public DecimalOperator add(long longVal) {
        oper = oper.add(longVal);
        return this;
    }

    @Override
    public DecimalOperator add(double doubleVal) {
        oper = oper.add(doubleVal);
        return this;
    }

    @Override
    public DecimalOperator subtract(BigDecimal bigDecimal) {
        oper = oper.subtract(bigDecimal);
        return this;
    }

    @Override
    public DecimalOperator subtract(int integer) {
        oper = oper.subtract(integer);
        return this;
    }

    @Override
    public DecimalOperator subtract(long longVal) {
        oper = oper.subtract(longVal);
        return this;
    }

    @Override
    public DecimalOperator subtract(double doubleVal) {
        oper = oper.subtract(doubleVal);
        return this;
    }

    @Override
    public DecimalOperator multiply(BigDecimal bigDecimal) {
        oper = oper.multiply(bigDecimal);
        return this;
    }

    @Override
    public DecimalOperator multiply(int integer) {
        oper = oper.multiply(integer);
        return this;
    }

    @Override
    public DecimalOperator multiply(long longVal) {
        oper = oper.multiply(longVal);
        return this;
    }

    @Override
    public DecimalOperator multiply(double doubleVal) {
        oper = oper.multiply(doubleVal);
        return this;
    }

    @Override
    public DecimalOperator divide(BigDecimal bigDecimal) {
        oper = oper.divide(bigDecimal);
        return this;
    }

    @Override
    public DecimalOperator divide(int integer) {
        oper = oper.divide(integer);
        return this;
    }

    @Override
    public DecimalOperator divide(long longVal) {
        oper = oper.divide(longVal);
        return this;
    }

    @Override
    public DecimalOperator divide(double doubleVal) {
        oper = oper.divide(doubleVal);
        return this;
    }

    @Override
    public BigDecimal decimal() {
        return oper.decimal();
    }

    @Override
    public boolean eq(int integer) {
        return oper.eq(integer);
    }

    @Override
    public boolean eq(long longVal) {
        return oper.eq(longVal);
    }

    @Override
    public boolean eq(double doubleVal) {
        return oper.eq(doubleVal);
    }

    @Override
    public boolean eq(BigDecimal bigDecimal) {
        return oper.eq(bigDecimal);
    }

    @Override
    public boolean lt(int integer) {
        return oper.lt(integer);
    }

    @Override
    public boolean lt(long longVal) {
        return oper.lt(longVal);
    }

    @Override
    public boolean lt(double doubleVal) {
        return oper.lt(doubleVal);
    }

    @Override
    public boolean lt(BigDecimal bigDecimal) {
        return oper.lt(bigDecimal);
    }

    @Override
    public boolean le(int integer) {
        return oper.le(integer);
    }

    @Override
    public boolean le(long longVal) {
        return oper.le(longVal);
    }

    @Override
    public boolean le(double doubleVal) {
        return oper.le(doubleVal);
    }

    @Override
    public boolean le(BigDecimal bigDecimal) {
        return oper.le(bigDecimal);
    }

    @Override
    public boolean gt(int integer) {
        return oper.gt(integer);
    }

    @Override
    public boolean gt(long longVal) {
        return oper.gt(longVal);
    }

    @Override
    public boolean gt(double doubleVal) {
        return oper.gt(doubleVal);
    }

    @Override
    public boolean gt(BigDecimal bigDecimal) {
        return oper.gt(bigDecimal);
    }

    @Override
    public boolean ge(int integer) {
        return oper.ge(integer);
    }

    @Override
    public boolean ge(long longVal) {
        return oper.ge(longVal);
    }

    @Override
    public boolean ge(double doubleVal) {
        return oper.ge(doubleVal);
    }

    @Override
    public boolean ge(BigDecimal bigDecimal) {
        return oper.ge(bigDecimal);
    }

    @Override
    public BigDecimal minVal(int integer) {
        return oper.minVal(integer);
    }

    @Override
    public BigDecimal minVal(long longVal) {
        return oper.minVal(longVal);
    }

    @Override
    public BigDecimal minVal(double doubleVal) {
        return oper.minVal(doubleVal);
    }

    @Override
    public BigDecimal minVal(BigDecimal bigDecimal) {
        return oper.minVal(bigDecimal);
    }

    @Override
    public BigDecimal maxVal(int integer) {
        return oper.maxVal(integer);
    }

    @Override
    public BigDecimal maxVal(long longVal) {
        return oper.maxVal(longVal);
    }

    @Override
    public BigDecimal maxVal(double doubleVal) {
        return oper.maxVal(doubleVal);
    }

    @Override
    public BigDecimal maxVal(BigDecimal bigDecimal) {
        return oper.maxVal(bigDecimal);
    }

    @Override
    public DecimalOperator min(int integer) {
        oper = oper.min(integer);
        return this;
    }

    @Override
    public DecimalOperator min(long longVal) {
        oper = oper.min(longVal);
        return this;
    }

    @Override
    public DecimalOperator min(double doubleVal) {
        oper = oper.min(doubleVal);
        return this;
    }

    @Override
    public DecimalOperator min(BigDecimal bigDecimal) {
        oper = oper.min(bigDecimal);
        return this;
    }

    @Override
    public DecimalOperator max(int integer) {
        oper = oper.max(integer);
        return this;
    }

    @Override
    public DecimalOperator max(long longVal) {
        oper = oper.max(longVal);
        return this;
    }

    @Override
    public DecimalOperator max(double doubleVal) {
        oper = oper.max(doubleVal);
        return this;
    }

    @Override
    public DecimalOperator max(BigDecimal bigDecimal) {
        oper = oper.max(bigDecimal);
        return this;
    }

    @Override
    public boolean isNegative() {
        return oper.isNegative();
    }

    @Override
    public DecimalOperator negate() {
        oper = oper.negate();
        return this;
    }

    @Override
    public String decimalString() {
        return oper.decimalString();
    }

    @Override
    public String decimalString(int scale) {
        return oper.decimalString(scale);
    }

    @Override
    public DecimalBuilder builder() {
        return oper.builder();
    }

    @Override
    public DecimalOperator builder(DecimalBuilder builder) {
        oper = oper.builder(builder);
        return this;
    }

    @Override
    public DecimalOperator mutable() {
        return this;
    }

    @Override
    public DecimalOperator immutable() {
        return this.oper;
    }
}
