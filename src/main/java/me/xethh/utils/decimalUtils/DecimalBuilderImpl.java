package me.xethh.utils.decimalUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author xethhung
 * Created on 9/20/2018
 */
public class DecimalBuilderImpl implements DecimalBuilder{
    private RoundingMode mode;
    private int scale;

    private DecimalBuilderImpl(RoundingMode mode, int scale){
        this.mode = mode;
        this.scale = scale;
    }

    protected static DecimalBuilder of(RoundingMode mode, int scale){
        return new DecimalBuilderImpl(mode,scale);
    }

    public DecimalBuilderImpl mode(RoundingMode mode){
        return new DecimalBuilderImpl(mode,scale);
    }
    public DecimalBuilderImpl scale(int scale){
        return new DecimalBuilderImpl(mode,scale);
    }

    @Override
    public RoundingMode mode() {
        return mode;
    }

    @Override
    public int scale() {
        return scale;
    }

    public BigDecimal format(BigDecimal bigDecimal){
        return bigDecimal.setScale(scale,mode);
    }

    public BigDecimal format(int integer){
        return new BigDecimal(integer+"").setScale(scale,mode);
    }

    public BigDecimal format(long longVal){
        return new BigDecimal(longVal+"").setScale(scale,mode);
    }

    public BigDecimal format(double doubleVal){
        return new BigDecimal(doubleVal+"").setScale(scale,mode);
    }

    @Override
    public DecimalOperator compute(BigDecimal bigDecimal) {
        return DecimalOperatorImpl.of(this,format(bigDecimal));
    }

    @Override
    public DecimalOperator compute(int integer) {
        return DecimalOperatorImpl.of(this, format(integer));
    }

    @Override
    public DecimalOperator compute(long longVal) {
        return DecimalOperatorImpl.of(this, format(longVal));
    }

    @Override
    public DecimalOperator compute(double doubleVal) {
        return DecimalOperatorImpl.of(this, format(doubleVal));
    }

    @Override
    public String toString() {
        return "DecimalBuilderImpl{" +
                "mode=" + mode +
                ", scale=" + scale +
                '}';
    }
}
