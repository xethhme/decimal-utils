package me.xethh.utils.decimalUtils;

import java.math.BigDecimal;

/**
 * @author xethhung
 * Created on 9/20/2018
 */
public interface DecimalOperator {
    DecimalOperator add(BigDecimal bigDecimal);
    DecimalOperator add(int integer);
    DecimalOperator add(long longVal);
    DecimalOperator add(double doubleVal);

    DecimalOperator subtract(BigDecimal bigDecimal);
    DecimalOperator subtract(int integer);
    DecimalOperator subtract(long longVal);
    DecimalOperator subtract(double doubleVal);

    DecimalOperator multiply(BigDecimal bigDecimal);
    DecimalOperator multiply(int integer);
    DecimalOperator multiply(long longVal);
    DecimalOperator multiply(double doubleVal);

    DecimalOperator divide(BigDecimal bigDecimal);
    DecimalOperator divide(int integer);
    DecimalOperator divide(long longVal);
    DecimalOperator divide(double doubleVal);
    BigDecimal decimal();

    boolean eq(int integer);
    boolean eq(long longVal);
    boolean eq(double doubleVal);
    boolean eq(BigDecimal bigDecimal);

    boolean lt(int integer);
    boolean lt(long longVal);
    boolean lt(double doubleVal);
    boolean lt(BigDecimal bigDecimal);

    boolean le(int integer);
    boolean le(long longVal);
    boolean le(double doubleVal);
    boolean le(BigDecimal bigDecimal);

    boolean gt(int integer);
    boolean gt(long longVal);
    boolean gt(double doubleVal);
    boolean gt(BigDecimal bigDecimal);

    boolean ge(int integer);
    boolean ge(long longVal);
    boolean ge(double doubleVal);
    boolean ge(BigDecimal bigDecimal);

    BigDecimal minVal(int integer);
    BigDecimal minVal(long longVal);
    BigDecimal minVal(double doubleVal);
    BigDecimal minVal(BigDecimal bigDecimal);

    BigDecimal maxVal(int integer);
    BigDecimal maxVal(long longVal);
    BigDecimal maxVal(double doubleVal);
    BigDecimal maxVal(BigDecimal bigDecimal);

    DecimalOperator min(int integer);
    DecimalOperator min(long longVal);
    DecimalOperator min(double doubleVal);
    DecimalOperator min(BigDecimal bigDecimal);

    DecimalOperator max(int integer);
    DecimalOperator max(long longVal);
    DecimalOperator max(double doubleVal);
    DecimalOperator max(BigDecimal bigDecimal);

    boolean isNegative();
    DecimalOperator negate();

    String decimalString();
    String decimalString(int scale);

    DecimalBuilder builder() ;

    DecimalOperator builder(DecimalBuilder builder) ;

    DecimalOperator mutable();
    DecimalOperator immutable();
}
