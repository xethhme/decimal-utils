package me.xethh.utils.decimalUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author xethhung
 * Created on 9/27/2018
 */
public class DecimalFactory {
    public static DecimalBuilder builder(){
        return DecimalBuilderImpl.of(RoundingMode.HALF_UP,2);
    }
    public static DecimalOperator immutableOperator(BigDecimal decimal){
        return DecimalOperatorImpl.of(builder(),decimal);
    }
    public static DecimalOperator immutableOperator(int integer){
        DecimalBuilder builder = builder();
        return DecimalOperatorImpl.of(builder,builder.format(integer));
    }
    public static DecimalOperator immutableOperator(long longVal){
        DecimalBuilder builder = builder();
        return DecimalOperatorImpl.of(builder,builder.format(longVal));
    }
    public static DecimalOperator immutableOperator(Double doubleVal){
        DecimalBuilder builder = builder();
        return DecimalOperatorImpl.of(builder,builder.format(doubleVal));
    }

    public static DecimalOperator mutableOperator(BigDecimal decimal){
        return immutableOperator(decimal).mutable();
    }
    public static DecimalOperator mutableOperator(int integer){
        return immutableOperator(integer).mutable();
    }
    public static DecimalOperator mutableOperator(long longVal){
        return immutableOperator(longVal).mutable();
    }
    public static DecimalOperator mutableOperator(Double doubleVal){
        return immutableOperator(doubleVal).mutable();
    }

    public static DecimalBuilder scale(int scale){
        return DecimalBuilderImpl.of(RoundingMode.HALF_UP,scale);
    }

    public static DecimalBuilder mode(RoundingMode roundingMode){
        return DecimalBuilderImpl.of(roundingMode,2);
    }

}
