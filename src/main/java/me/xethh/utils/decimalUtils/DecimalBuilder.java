package me.xethh.utils.decimalUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author xethhung
 * Created on 9/20/2018
 */
public interface DecimalBuilder {

    DecimalBuilder mode(RoundingMode mode);
    DecimalBuilder scale(int scale);
    RoundingMode mode();
    int scale();

    BigDecimal format(BigDecimal bigDecimal);
    BigDecimal format(int integer);
    BigDecimal format(long longVal);
    BigDecimal format(double doubleVal);

    DecimalOperator compute(BigDecimal bigDecimal);
    DecimalOperator compute(int integer);
    DecimalOperator compute(long longVal);
    DecimalOperator compute(double doubleVal);

}
