package me.xethh.utils;

import me.xethh.utils.decimalUtils.DecimalBuilder;
import me.xethh.utils.decimalUtils.DecimalFactory;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


/**
 * Unit test for simple App.
 */
public class TestBigDecimalFromBuilder
{
    @Test
    public void tessCreateBuilder()
    {
        assertEquals(RoundingMode.HALF_UP,DecimalFactory.builder().mode());
        assertEquals(2,DecimalFactory.builder().scale());

        assertEquals(RoundingMode.HALF_UP,DecimalFactory.scale(4).mode());
        assertEquals(4,DecimalFactory.scale(4).scale());

        assertEquals(RoundingMode.HALF_DOWN,DecimalFactory.mode(RoundingMode.HALF_DOWN).mode());
        assertEquals(2,DecimalFactory.mode(RoundingMode.HALF_DOWN).scale());

        DecimalBuilder builder = DecimalFactory.builder();
        assertEquals(RoundingMode.HALF_DOWN,builder.mode(RoundingMode.HALF_DOWN).mode());
        assertEquals(2,builder.mode(RoundingMode.HALF_DOWN).scale());

        assertEquals(RoundingMode.HALF_UP,builder.scale(5).mode());
        assertEquals(5,builder.scale(5).scale());

    }

    /**
     * Rigorous Test :-)
     */
    @Test
    public void testConvertingInt()
    {
        BigDecimal bb = new BigDecimal("0.005");
        DecimalBuilder builder = DecimalFactory.scale(4);
        for(int i=0;i<10000;i++){
            String a = new BigDecimal(i + "").multiply(bb).setScale(4).toString();
            // System.out.println(a);
            String b = builder.compute(0.005).multiply(i).decimalString();
            // System.out.println(b);
            assertEquals(a,b);
        }
    }

    @Test
    public void testConvertingLong()
    {
        BigDecimal bb = new BigDecimal("0.005");
        DecimalBuilder builder = DecimalFactory.scale(4);
        for(long i=0;i<10000;i++){
            assertEquals(bb.multiply(new BigDecimal(i+"")).setScale(4).toString(),builder.compute(0.005).multiply(new BigDecimal(i)).decimalString());
        }
    }

    @Test
    public void testScaleAndRound()
    {
        BigDecimal b1 = new BigDecimal("0.005");
        BigDecimal b2 = new BigDecimal(0.005);

        assertEquals(b1.toString(),DecimalFactory.scale(3).format(b1).toString());
        assertNotEquals(b2.toString(),DecimalFactory.scale(3).format(b2).toString());
    }

}
