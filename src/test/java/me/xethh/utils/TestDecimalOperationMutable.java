package me.xethh.utils;

import me.xethh.utils.decimalUtils.DecimalBuilder;
import me.xethh.utils.decimalUtils.DecimalFactory;
import me.xethh.utils.decimalUtils.DecimalOperator;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * @author xethhung
 * Created on 9/27/2018
 */
public class TestDecimalOperationMutable {

    @Test
    public void testMutability(){
        assertEquals("7.50",DecimalFactory.mutableOperator(1).add(10).add(200).subtract(201).multiply(3).divide(4).decimalString());
    }
    @Test
    public void testOperation(){
        assertEquals("400.00",DecimalFactory.mutableOperator(new BigDecimal("200.00")).multiply(2).decimalString());
        assertEquals("400.00",DecimalFactory.mutableOperator(200.00).multiply(2).decimalString());
        assertEquals("400.00",DecimalFactory.mutableOperator(200).multiply(2).decimalString());
        assertEquals("400.00",DecimalFactory.mutableOperator(200l).multiply(2).decimalString());

        assertEquals("400.00",DecimalFactory.mutableOperator(200).multiply(new BigDecimal(2.0+"")).decimalString());
        assertEquals("400.00",DecimalFactory.mutableOperator(200).multiply(2).decimalString());
        assertEquals("400.00",DecimalFactory.mutableOperator(200).multiply(2l).decimalString());
        assertEquals("400.00",DecimalFactory.mutableOperator(200).multiply(2.0).decimalString());

        assertEquals("100.00",DecimalFactory.mutableOperator(new BigDecimal("200.00")).divide(2).decimalString());
        assertEquals("100.00",DecimalFactory.mutableOperator(200.00).divide(2).decimalString());
        assertEquals("100.00",DecimalFactory.mutableOperator(200).divide(2).decimalString());
        assertEquals("100.00",DecimalFactory.mutableOperator(200l).divide(2).decimalString());

        assertEquals("100.00",DecimalFactory.mutableOperator(200).divide(new BigDecimal(2.0+"")).decimalString());
        assertEquals("100.00",DecimalFactory.mutableOperator(200).divide(2).decimalString());
        assertEquals("100.00",DecimalFactory.mutableOperator(200).divide(2l).decimalString());
        assertEquals("100.00",DecimalFactory.mutableOperator(200).divide(2.0).decimalString());

        assertEquals("202.01",DecimalFactory.mutableOperator(200).add(new BigDecimal(2.011+"")).decimalString());
        assertEquals("202.00",DecimalFactory.mutableOperator(200).add(2).decimalString());
        assertEquals("202.00",DecimalFactory.mutableOperator(200).add(2l).decimalString());
        assertEquals("202.02",DecimalFactory.mutableOperator(200).add(2.015).decimalString());

        assertEquals("197.99",DecimalFactory.mutableOperator(200).subtract(new BigDecimal(2.011+"")).decimalString());
        assertEquals("198.00",DecimalFactory.mutableOperator(200).subtract(2).decimalString());
        assertEquals("198.00",DecimalFactory.mutableOperator(200).subtract(2l).decimalString());
        assertEquals("197.98",DecimalFactory.mutableOperator(200).subtract(2.015).decimalString());
    }

    @Test
    public void testRounding(){
        assertEquals("200.00",DecimalFactory.mutableOperator(new BigDecimal("200.000")).decimalString());
        assertEquals("200.00",DecimalFactory.mutableOperator(new BigDecimal("200.001")).decimalString());
        assertEquals("200.00",DecimalFactory.mutableOperator(new BigDecimal("200.002")).decimalString());
        assertEquals("200.00",DecimalFactory.mutableOperator(new BigDecimal("200.003")).decimalString());
        assertEquals("200.00",DecimalFactory.mutableOperator(new BigDecimal("200.004")).decimalString());
        assertEquals("200.01",DecimalFactory.mutableOperator(new BigDecimal("200.005")).decimalString());
        assertEquals("200.01",DecimalFactory.mutableOperator(new BigDecimal("200.006")).decimalString());
        assertEquals("200.01",DecimalFactory.mutableOperator(new BigDecimal("200.007")).decimalString());
        assertEquals("200.01",DecimalFactory.mutableOperator(new BigDecimal("200.008")).decimalString());
        assertEquals("200.01",DecimalFactory.mutableOperator(new BigDecimal("200.009")).decimalString());
    }

    @Test
    public void testCompareOperation(){
        DecimalBuilder fm1 = DecimalFactory.builder();
        assertEquals(false, fm1.compute(3.2).eq(3));
        assertEquals(false, fm1.compute(3.2).eq(3L));
        assertEquals(true, fm1.compute(3.2).eq(3.2));
        assertEquals(false, fm1.compute(3.2).eq(3.21));

        assertEquals(true, fm1.compute(3.211).eq(3.211));
        assertEquals(true, fm1.compute(3.2114).eq(3.211));
        assertEquals(true, fm1.compute(3.2115).eq(3.212));
        assertEquals(true, fm1.compute(3.2116).eq(3.212));

        fm1=fm1.scale(3);
        assertEquals(false, fm1.compute(3.211).gt(3.211));
        assertEquals(false, fm1.compute(3.2114).gt(3.211));
        assertEquals(false, fm1.compute(3.2115).gt(3.212));
        assertEquals(false, fm1.compute(3.2116).gt(3.212));
        assertEquals(false, fm1.compute(3.2121).gt(3.212));
        assertEquals(false, fm1.compute(3.2124).gt(3.212));
        assertEquals(true, fm1.compute(3.2125).gt(3.212));
        assertEquals(true, fm1.compute(3.2126).gt(3.212));

        assertEquals(true, fm1.compute(3.211).ge(3.211));
        assertEquals(true, fm1.compute(3.2114).ge(3.211));
        assertEquals(true, fm1.compute(3.2115).ge(3.212));
        assertEquals(true, fm1.compute(3.2116).ge(3.212));
        assertEquals(true, fm1.compute(3.2121).ge(3.212));
        assertEquals(true, fm1.compute(3.2124).ge(3.212));
        assertEquals(true, fm1.compute(3.2125).ge(3.212));
        assertEquals(true, fm1.compute(3.2126).ge(3.212));

        assertEquals(false, fm1.compute(3.211).lt(3.211));
        assertEquals(false, fm1.compute(3.2114).lt(3.211));
        assertEquals(false, fm1.compute(3.2115).lt(3.212));
        assertEquals(false, fm1.compute(3.2116).lt(3.212));
        assertEquals(false, fm1.compute(3.2121).lt(3.212));
        assertEquals(false, fm1.compute(3.2124).lt(3.212));
        assertEquals(false, fm1.compute(3.2125).lt(3.212));
        assertEquals(false, fm1.compute(3.2126).lt(3.212));

        assertEquals(true, fm1.compute(3.211).le(3.211));
        assertEquals(true, fm1.compute(3.2114).le(3.211));
        assertEquals(true, fm1.compute(3.2115).le(3.212));
        assertEquals(true, fm1.compute(3.2116).le(3.212));
        assertEquals(true, fm1.compute(3.2121).le(3.212));
        assertEquals(true, fm1.compute(3.2124).le(3.212));
        assertEquals(false, fm1.compute(3.2125).le(3.212));
        assertEquals(false, fm1.compute(3.2126).le(3.212));
    }

    @Test
    public void testMinMax(){
        DecimalOperator oper = DecimalFactory.mutableOperator(1);
        assertEquals("1.00", oper.minVal(1.005).toString());
        assertEquals("1.01", oper.maxVal(1.005).toString());

        assertEquals("1.00", oper.min(1.005).decimalString());
        assertEquals("1.01", oper.max(1.005).decimalString());

    }

    @Test
    public void testNegate(){
        DecimalOperator oper = DecimalFactory.mutableOperator(1);
        assertEquals(false, oper.isNegative());
        assertEquals(true, DecimalFactory.mutableOperator(-1).isNegative());

        assertEquals(true, oper.negate().isNegative());
    }
}
